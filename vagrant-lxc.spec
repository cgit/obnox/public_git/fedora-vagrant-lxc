%global vagrant_plugin_name vagrant-lxc

Name: %{vagrant_plugin_name}
Version: 1.1.0
Release: 5%{?dist}
Summary: LXC provider for vagrant
Group: Development/Languages
License: MIT
URL: https://github.com/fgrehm/vagrant-lxc
Source0: https://rubygems.org/gems/%{vagrant_plugin_name}-%{version}.gem

# script needed to generate the vagrant-lxc sudo wrapper script from template.
# part of this srpm
Source1: create_wrapper.rb

Patch1: vagrant-lxc-sudo-wrapper.patch

Requires(pre): shadow-utils
Requires(posttrans): vagrant
Requires(preun): vagrant
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: lxc
Requires: vagrant

BuildRequires: vagrant
BuildRequires: rubygem(rspec) < 3
BuildArch: noarch

%description
LXC provider for vagrant.

%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{vagrant_plugin_name}-%{version}

%patch1 -p1

gem spec %{SOURCE0} -l --ruby > %{vagrant_plugin_name}.gemspec


%build
gem build %{vagrant_plugin_name}.gemspec
%vagrant_plugin_install

%install
mkdir -p %{buildroot}%{vagrant_plugin_dir}
cp -a .%{vagrant_plugin_dir}/* \
        %{buildroot}%{vagrant_plugin_dir}/

ruby -I /usr/share/vagrant/lib %{SOURCE1} %{buildroot}%{vagrant_plugin_instdir}/templates
install -m 0555 ./vagrant-lxc-wrapper %{buildroot}%{vagrant_plugin_instdir}/scripts/vagrant-lxc-wrapper

echo "%vagrant ALL=(root) NOPASSWD: %{vagrant_plugin_instdir}/scripts/vagrant-lxc-wrapper" > ./sudoers_file
mkdir -p %{buildroot}%{_sysconfdir}/sudoers.d
install -m 0440 ./sudoers_file %{buildroot}%{_sysconfdir}/sudoers.d/vagrant-lxc

# TODO: enable check.
#  currently rubygem-hitimes is missing, but
#  Problems building under f21 and newer:
#  https://bugzilla.redhat.com/show_bug.cgi?id=1068980
#
#%check
#pushd .%{vagrant_plugin_instdir}
#sed -i '/git/ s/^/#/' Gemfile
#
#bundle exec rspec2 spec
#popd

%pre
getent group vagrant >/dev/null || groupadd -r vagrant

%posttrans
%vagrant_plugin_register %{vagrant_plugin_name}


%preun
%vagrant_plugin_unregister %{vagrant_plugin_name}

%files
%dir %{vagrant_plugin_instdir}
%license %{vagrant_plugin_instdir}/LICENSE.txt
%doc %{vagrant_plugin_instdir}/README.md
%{vagrant_plugin_libdir}
%{vagrant_plugin_instdir}/locales
%exclude %{vagrant_plugin_cache}
%exclude %{vagrant_plugin_instdir}/.gitignore
%{vagrant_plugin_spec}
%{_sysconfdir}/sudoers.d/vagrant-lxc

%exclude %{vagrant_plugin_instdir}/.rspec
%exclude %{vagrant_plugin_instdir}/.travis.yml
%exclude %{vagrant_plugin_instdir}/.vimrc
%{vagrant_plugin_instdir}/Gemfile.lock
%{vagrant_plugin_instdir}/scripts/lxc-template
%{vagrant_plugin_instdir}/scripts/pipework
%{vagrant_plugin_instdir}/scripts/vagrant-lxc-wrapper
%{vagrant_plugin_instdir}/templates/sudoers.rb.erb


%files doc
%doc %{vagrant_plugin_docdir}
%doc %{vagrant_plugin_instdir}/CHANGELOG.md
%doc %{vagrant_plugin_instdir}/BOXES.md
%doc %{vagrant_plugin_instdir}/CONTRIBUTING.md
%{vagrant_plugin_instdir}/Rakefile
%{vagrant_plugin_instdir}/Gemfile
%{vagrant_plugin_instdir}/vagrant-lxc.gemspec
%{vagrant_plugin_instdir}/spec
%{vagrant_plugin_instdir}/Guardfile
%{vagrant_plugin_instdir}/tasks/spec.rake
%{vagrant_plugin_instdir}/vagrant-spec.config.rb

%changelog
* Tue Jan 27 2015 Michael Adam <madam@redhat.com> - 1.1.0-5
- Cleanup specfile.

* Mon Jan 26 2015 Michael Adam <madam@redhat.com> - 1.1.0-4
- Ship precreated sudo-wrapper and sudoers file.

* Mon Jan 26 2015 Michael Adam <madam@redhat.com> - 1.1.0-3
- Capitalize summary and description.
- Fix sudo wrapper and "vagrant lxc sudoers" mechansim from upstream.

* Mon Jan 26 2015 Michael Adam <madam@redhat.com> - 1.1.0-2
- Move some files from -doc to main package.

* Sat Jan 24 2015 Michael Adam <madam@redhat.com> - 1.1.0-1
- Initial package for Fedora
